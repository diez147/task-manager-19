package ru.tsc.babeshko.tm.model;

import java.util.UUID;

public class AbstractModel {

    protected String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}