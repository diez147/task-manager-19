package ru.tsc.babeshko.tm.service;

import ru.tsc.babeshko.tm.api.service.IAuthService;
import ru.tsc.babeshko.tm.api.service.IUserService;
import ru.tsc.babeshko.tm.exception.entity.user.AccessDeniedException;
import ru.tsc.babeshko.tm.exception.field.LoginEmptyException;
import ru.tsc.babeshko.tm.exception.field.PasswordEmptyException;
import ru.tsc.babeshko.tm.model.User;
import ru.tsc.babeshko.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    private String userId;

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(String login, String password, String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

}