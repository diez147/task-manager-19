package ru.tsc.babeshko.tm.service;

import ru.tsc.babeshko.tm.api.repository.IUserRepository;
import ru.tsc.babeshko.tm.api.service.IUserService;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.exception.entity.user.EmailExistsException;
import ru.tsc.babeshko.tm.exception.entity.user.LoginExistsException;
import ru.tsc.babeshko.tm.exception.entity.user.UserNotFoundException;
import ru.tsc.babeshko.tm.exception.field.*;
import ru.tsc.babeshko.tm.model.User;
import ru.tsc.babeshko.tm.util.HashUtil;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    public UserService(IUserRepository repository) {
        super(repository);
    }

    @Override
    public User findByLogin(String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

     @Override
    public User removeByLogin(String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.removeByLogin(login);
    }

    @Override
    public User findByEmail(String email) {
        if (email == null || email.isEmpty()) throw new LoginEmptyException();
        return repository.findByEmail(email);
    }

    @Override
    public User create(String login, String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return repository.add(user);
    }

    @Override
    public User create(String login, String password, String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailExistsException();
        if (isLoginExist(login)) throw new LoginExistsException();
        final User user = create(login, password);
        if (user == null) throw new UserNotFoundException();
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(String login, String password, Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        final User user = create(login, password);
        if (user == null) throw new UserNotFoundException();
        user.setRole(role);
        return user;
    }

    @Override
    public User setPassword(String userId, String password) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(String userId, String firstName, String lastName, String middleName) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        final User user = findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public boolean isLoginExist(String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

}