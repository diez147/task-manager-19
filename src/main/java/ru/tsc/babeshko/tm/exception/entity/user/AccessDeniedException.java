package ru.tsc.babeshko.tm.exception.entity.user;

import ru.tsc.babeshko.tm.exception.entity.AbstractEntityNotFoundException;

public class AccessDeniedException extends AbstractEntityNotFoundException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}