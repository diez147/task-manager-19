package ru.tsc.babeshko.tm.command.system;


import ru.tsc.babeshko.tm.api.model.ICommand;
import ru.tsc.babeshko.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsListCommand extends AbstractSystemCommand {

    public static final String NAME = "command";

    public static final String DESCRIPTION = "Show command list.";

    public static final String ARGUMENT = "-cmd";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}