package ru.tsc.babeshko.tm.command.user;

import ru.tsc.babeshko.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String NAME = "user-change-password";

    public static final String DESCRIPTION = "Change user password.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("[ENTER NEW PASSWORD:]");
        final String password = TerminalUtil.nextLine();
        final String userId = getAuthService().getUserId();
        getUserService().setPassword(userId, password);
    }

}