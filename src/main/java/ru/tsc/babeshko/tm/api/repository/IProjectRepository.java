package ru.tsc.babeshko.tm.api.repository;

import ru.tsc.babeshko.tm.model.Project;

public interface IProjectRepository extends IRepository<Project>{

    Project create(String name);

    Project create(String name, String description);

}