package ru.tsc.babeshko.tm.api.service;

import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IService<Project>{

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, String description, Date dateBegin, Date dateEnd);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

}