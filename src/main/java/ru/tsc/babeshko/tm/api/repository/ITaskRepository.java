package ru.tsc.babeshko.tm.api.repository;

import ru.tsc.babeshko.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task>{

    Task create(String name);

    List<Task> findAllByProjectId(String projectId);

    Task create(String name, String description);

}