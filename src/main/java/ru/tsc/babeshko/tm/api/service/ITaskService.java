package ru.tsc.babeshko.tm.api.service;

import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task>{

    List<Task> findAllByProjectId(String project);

    Task create(String name);

    Task create(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}